import logging
import sys
import os
import grpc
import cmd_pb2
import cmd_pb2_grpc


def run(command):
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel(f"{os.getenv('HOST_IP')}:50051") as channel:
        stub = cmd_pb2_grpc.CommandHandlerStub(channel)
        response = stub.RunCommand(cmd_pb2.Command(command=command))
    print("Output received: " + response.output)
    print(f"Exit code received: {str(response.exitCode)}")


if __name__ == '__main__':
    if len(sys.argv) > 1:
        command = sys.argv[1]
    else:
        command = os.getenv('COMMAND')
    logging.basicConfig()
    run(command)
