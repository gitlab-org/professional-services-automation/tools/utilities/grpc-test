FROM python:3.8.13-slim-buster

COPY command_server.py /opt/command_server.py
COPY cmd_pb2_grpc.py /opt/cmd_pb2_grpc.py
COPY cmd_pb2.py /opt/cmd_pb2.py

RUN pip install grpcio grpcio-tools

WORKDIR /opt

EXPOSE 50051

ENTRYPOINT ["python", "command_server.py"]