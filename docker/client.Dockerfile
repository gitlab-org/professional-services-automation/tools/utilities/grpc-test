FROM python:3.8.13-slim-buster


ARG HOST_IP_ARG=0.0.0.0
ENV COMMAND 'echo hello'
ENV HOST_IP=$HOST_IP_ARG

COPY command_client.py /opt/command_client.py
COPY cmd_pb2_grpc.py /opt/cmd_pb2_grpc.py
COPY cmd_pb2.py /opt/cmd_pb2.py

RUN pip install grpcio grpcio-tools

WORKDIR /opt

CMD ["/bin/bash", "-c", "--", "while true; do sleep 30; done;"]