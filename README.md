# Running commands through gRPC

Very quick test of gRPC to try running commands on a docker container from another location through gRPC

## Setup

```
git clone <this-repo>
poetry install
```

### Building the docker images

```bash
# client
cp docker/client.Dockerfile ./Dockerfile
sudo podman build -t grpc-client -e .

# server
cp docker/server.Dockerfile ./Dockerfile
sudo podman build -t grpc-server .
```

### Running the docker compose

```bash
cd docker
# If you are using podman
HOST_IP=<your-host-ip> sudo -E docker-compose -H unix:///run/podman/podman.sock up
# If you are using docker
HOST_IP=<your-host-ip> sudo -E docker-compose up
```

### Rebuilding protobuf python files (optional)

`cmd.proto` needs to be built into python files before it can be used.
**This is already stored in the repo**, but if you need to change the .proto file,
run the following command from the root of this directory:

```
poetry run python -m grpc_tools.protoc --proto_path=./protos --python_out=. --grpc_python_out=. ./protos/cmd.proto
```

## Testing the client/server connection

```bash
sudo podman exec docker_client_1 python command_client.py 'echo hello'
```

should output:
```
Output received: hello

Exit code received: 0
```

What is happening here is you are passing a command, in this case `echo hello`, to the server to run.

The proto file defines a few different data structures. One of those data structures is a "response" message.
That message expects an output _string_ and an exist code _int_.